@ComparacionLaptops @regresion
Feature: Validacion de funcionamiento de comparacion de Laptops en gadgetsnow.com

  @Scenario1
  Scenario: Exportar sumario de comparacion entre dos laptops
    Given ingreso al portal de gadgetsnow a la pagina de comparacion de laptops
		And  ingreso marca en input de seleccion de laptops vacio "Lenovo E41-80"
		And selecciono primera opcion en la lista
		And  ingreso marca en input de seleccion de laptops vacio "Lenovo B40-80"
		And selecciono primera opcion en la lista
		When presiono boton compare
		Then tomar valores de tabla y exportarla a excel