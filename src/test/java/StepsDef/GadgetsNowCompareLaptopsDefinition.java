package StepsDef;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import Pages.GadgetsNowCompareLaptopsPage;
import io.cucumber.java.After;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class GadgetsNowCompareLaptopsDefinition {
	
	WebDriver driver = null;
	GadgetsNowCompareLaptopsPage gadgetsNowCompareLaptopsPage = new GadgetsNowCompareLaptopsPage(driver);
	
	public GadgetsNowCompareLaptopsDefinition() {
	}
	
	@Given("ingreso al portal de gadgetsnow a la pagina de comparacion de laptops")
	public void el_navegador_se_abre() {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\Martin\\eclipse-workspace\\MiTemplateBase\\src\\test\\resources\\Driver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get("https://www.gadgetsnow.com/compare-laptops");
	}

	@When("ingreso marca en input de seleccion de laptops vacio {string}")
	public void ingreso_marca_en_input_de_seleccion_de_laptops_vacio(String text) {
		System.out.println(text);
		gadgetsNowCompareLaptopsPage = new GadgetsNowCompareLaptopsPage(driver);
		gadgetsNowCompareLaptopsPage.inputTextSearchLaptops(text);
		
	}
	
	
	@And("selecciono primera opcion en la lista")
	public void selecciono_primera_opcion_en_la_lista() {
		gadgetsNowCompareLaptopsPage = new GadgetsNowCompareLaptopsPage(driver);
		gadgetsNowCompareLaptopsPage.SelectFirstOptionOnLaptopsList();
		
	}

	@When("presiono boton compare")
	public void presiono_boton_compare() {
		gadgetsNowCompareLaptopsPage = new GadgetsNowCompareLaptopsPage(driver);
		gadgetsNowCompareLaptopsPage.clickButtonCompare();
	}
	
	
	@Then("tomar valores de tabla y exportarla a excel")
	public void tomar_valores_de_tabla_y_exportarla_a_excel() throws IOException {
		gadgetsNowCompareLaptopsPage.createComparisonExcel();
	}
	
	@After("@regresion")
	public void tearDown() {
	    driver.quit();
	}

}
