package Common;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;

public class BaseClass{
	
    public static WebDriver driver;
    
    protected BaseClass(WebDriver driver){
        this.driver=driver;
    }

    
    public void click(WebElement element) {
    	element.click();
    }
    
	public void inputText(String textoAIngresar, WebElement element) {
		System.out.println(textoAIngresar);
		element.sendKeys(textoAIngresar);
	}
	
	public void waitForElementToAppear(WebElement element) {
		WebDriverWait wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.visibilityOf(element));
	}
	//usando la libreria Apache POI
	public void obtenerDataYExportarla(WebElement element) throws IOException {
		String PathTillProject = System.getProperty("user.dir");
		File file = new File(PathTillProject + "/xlsGenerados/Comparison Excel.xls");
		XSSFWorkbook wb = new XSSFWorkbook();
		XSSFSheet sh = wb.createSheet("Comparison");
		
		WebElement table= element;
		List<WebElement> totalRows = table.findElements(By.tagName("tr"));
		for(int row=0;row<totalRows.size(); row++) {
			XSSFRow rowValue = sh.createRow(row);
			List<WebElement>th =totalRows.get(row).findElements(By.tagName("th"));
			List<WebElement>td=totalRows.get(row).findElements(By.tagName("td"));
			List<WebElement>totalColumns=new ArrayList<WebElement>();
			totalColumns.addAll(th);
			totalColumns.addAll(td);
			for(int col=0;col<totalColumns.size();col++) {
				String cellValue = totalColumns.get(col).getText().trim();
				System.out.print(cellValue +"\t");
				rowValue.createCell(col).setCellValue(cellValue);
			}
			System.out.println();
		}
		FileOutputStream fos = new FileOutputStream(file);
		wb.write(fos);
		wb.close();
	}
	
	@AfterClass(alwaysRun = true)
	protected void tearDown() {
	    driver.quit();
	    driver = null;
	}
}