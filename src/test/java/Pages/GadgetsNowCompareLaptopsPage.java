package Pages;
import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import Common.BaseClass;

public class GadgetsNowCompareLaptopsPage extends BaseClass{
	
	public GadgetsNowCompareLaptopsPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "(//span[contains(text(),'Select')])[1]")
	WebElement plusBox;
	
	@FindBy(xpath = "//input[@placeholder='Search Laptops']")
	WebElement inputSearchLaptops;
	
	//Este xpath toma el primer elemento de la lista que se despliega luego de buscar
	@FindBy(xpath = "(//input[@placeholder='Search Laptops']/../following-sibling::div/*/li)[1]")
	WebElement firstLaptopOnList;
	
	@FindBy(xpath = "//*[text()='compare']")
	WebElement buttonCompare;
	
	@FindBy(xpath = "(//table)[1]")
	WebElement comparisonTable;

	public void inputTextSearchLaptops(String text) {
		System.out.println(text);
		//antes de hacer alguna accion con los elementos se espera que aparezcan
		waitForElementToAppear(plusBox);
		click(plusBox);
		
		waitForElementToAppear(inputSearchLaptops);
		inputText(text,inputSearchLaptops);
	}

	public void SelectFirstOptionOnLaptopsList() {
		
		waitForElementToAppear(firstLaptopOnList);
		click(firstLaptopOnList);
	}

	public void clickButtonCompare() {
		
		waitForElementToAppear(buttonCompare);
		click(buttonCompare);
		
	}

	public void createComparisonExcel() throws IOException {
		
		waitForElementToAppear(comparisonTable);
		obtenerDataYExportarla(comparisonTable);
		
	}
}